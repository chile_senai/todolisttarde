package br.senai.sp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;

import javax.management.RuntimeErrorException;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class SubtarefaDaoJDBC {
	private Connection conexao;

	@Autowired
	public SubtarefaDaoJDBC(DataSource datasource) {
		try {
			conexao = datasource.getConnection();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public void excluir(Long idSubtarefa) {
		try {
			PreparedStatement stmt = 
					conexao.prepareStatement
						("delete from subtarefa where id = ?");
			stmt.setLong(1, idSubtarefa);
			stmt.execute();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
