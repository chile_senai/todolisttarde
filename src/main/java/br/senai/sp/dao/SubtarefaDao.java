package br.senai.sp.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.senai.sp.modelo.Subtarefa;
import br.senai.sp.modelo.Tarefa;

@Repository
public class SubtarefaDao {
	@PersistenceContext
	private EntityManager manager;

	@Transactional
	public void inserir(Long idTarefa, Subtarefa subtarefa) {
		// Tarefa tarefa = manager.find(Tarefa.class, idTarefa);
		Tarefa tarefa = new Tarefa();
		tarefa.setId(idTarefa);
		subtarefa.setTarefa(tarefa);
		manager.persist(subtarefa);
	}

	public Subtarefa buscarSubtarefa(Long idSubtarefa) {
		return manager.find(Subtarefa.class, idSubtarefa);
	}

	@Transactional
	public void marcarFeito(Long idSubtarefa, boolean valor) {
		Subtarefa subtarefa = buscarSubtarefa(idSubtarefa);
		subtarefa.setFeita(valor);
		manager.merge(subtarefa);
	}

	@Transactional
	public void excluir(Long idSubtarefa) {
		Subtarefa subtarefa = buscarSubtarefa(idSubtarefa);
		Tarefa tarefa = subtarefa.getTarefa();
		tarefa.getSubtarefas().remove(subtarefa);
		manager.merge(tarefa);
	}
}
