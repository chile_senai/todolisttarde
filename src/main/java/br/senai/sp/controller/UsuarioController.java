package br.senai.sp.controller;

import java.net.URI;
import java.util.HashMap;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.auth0.jwt.JWTSigner;

import br.senai.sp.dao.UsuarioDao;
import br.senai.sp.modelo.TokenJWT;
import br.senai.sp.modelo.Usuario;

@RestController
public class UsuarioController {
	@Autowired
	private UsuarioDao dao;
	
	public static final String EMISSOR="SENAI";
	public static final String SECRET = "T@d@listSENAI";

	@RequestMapping(value = "/usuario", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Usuario> inserirUsuario(@RequestBody Usuario usuario) {
		try {
			dao.inserir(usuario);
			return ResponseEntity.created(new URI("/usuario/" + usuario.getId())).body(usuario);
		} catch (ConstraintViolationException e) {
			e.printStackTrace();
			return new ResponseEntity<Usuario>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Usuario>(HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<TokenJWT> logar(@RequestBody Usuario usuario) {
		usuario = dao.logar(usuario);
		if (usuario != null) {
			HashMap<String, Object> claims = new HashMap<String, Object>();
			claims.put("iss", EMISSOR);
			claims.put("id_usuario", usuario.getId());
			claims.put("nome_usuario", usuario.getNome());
			// hora atual em segundos
			long horaAtual = System.currentTimeMillis()/1000;
			// expira��o do token (3600 segundos)
			long horaExpiracao = horaAtual + 3600;
			claims.put("iat", horaAtual);
			claims.put("exp", horaExpiracao);
			JWTSigner signer = new JWTSigner(SECRET);
			TokenJWT tokenJwt = new TokenJWT();
			tokenJwt.setToken(signer.sign(claims));
			return ResponseEntity.ok(tokenJwt);
		} else {
			return new ResponseEntity<TokenJWT>(HttpStatus.UNAUTHORIZED);
		}
	}
}
