package br.senai.sp.controller;

import java.net.URI;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.senai.sp.dao.SubtarefaDao;
import br.senai.sp.dao.SubtarefaDaoJDBC;
import br.senai.sp.modelo.Subtarefa;

@RestController
public class SubtarefaController {
	@Autowired
	private SubtarefaDao daoSub;
	@Autowired
	private SubtarefaDaoJDBC daoJdbc;

	@RequestMapping(value = "/tarefa/{idTarefa}/subtarefa", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Subtarefa> criarSubtarefa(@PathVariable Long idTarefa, @RequestBody Subtarefa subtarefa) {
		try {
			daoSub.inserir(idTarefa, subtarefa);
			return ResponseEntity.created(URI.create("/subtarefa/" + subtarefa.getId())).body(subtarefa);
		} catch (ConstraintViolationException e) {
			e.printStackTrace();
			return new ResponseEntity<Subtarefa>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Subtarefa>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/subtarefa/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Subtarefa buscarItemTarefa(@PathVariable("id") long idSubtarefa) {
		return daoSub.buscarSubtarefa(idSubtarefa);
	}
	
	@RequestMapping(value="/subtarefa/{id}", 
			method=RequestMethod.PUT, 
			consumes=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Void> marcarFeito
	(@PathVariable("id") Long idSubtarefa,@RequestBody Subtarefa subtarefa){
		try {
			boolean valor = subtarefa.isFeita();
			daoSub.marcarFeito(idSubtarefa, valor);
			HttpHeaders header = new HttpHeaders();
			header.setLocation(URI.create("/subtarefa/"+idSubtarefa));
			return new ResponseEntity<Void>(header,HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/subtarefa/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> excluirItem(@PathVariable("id") long idItemTarefa) {
		daoJdbc.excluir(idItemTarefa);
		return ResponseEntity.noContent().build();
	}
}
